## Create a .gitlab-ci.yml file to see CI in action

### Theme

Create iterations to plan and track work into timeboxes, and create a board at the group level so that you can plan work into each sprint across all projects.

### Tasks and Steps

# Step 1: Create Iteration Cadences

1. Use the breadcrumbs to navigate to the **Tanuki-Racing-Group** parent group of your Tanuki Racing project
  
2. Use the **Plan > Iterations** menu to open the iterations page.  Click **New iteration cadence**, then add this information:
  - **Title**: _Standard 2 week sprint_
  - **Description**: _Creating a standard 2 week sprint cadence for dev teams_
  - **Automatic scheduling**: keep the checkbox **checked** to _Enable automatic scheduling_
  - **Automation start date**: choose today's date
  - **Duration**: choose **2** from the dropdown menu
  - **Upcoming iterations**: choose **10** from the dropdown menu
  - **Roll over issues**: leave the box **unchecked** to disable roll over

Then click **Create cadence** 

Notice that the Iterations page now shows the defined iteration cadence with 10 individual iterations created.  We will use these iterations for sprint planning purposes.
  

# Step 2: Create a group level issue board

1. Use the **Plan > Issue boards** menu to navigate to the group level issue boards.  Note: you should still be in the **Tanuki-Racing-Group** parent group of your Tanuki Racing project

1. Click the **New board** button next to **Development** at the top of the page.  Enter the **Title** _Sprint Planning Board_.  Then click **Create board**.
 
1. Click **Create list** in the upper right-hand corner.  
  - For **Scope**, choose **Iteration**
  - For **Value**, choose the first iteration in the dropdown list.

Then click **Add to Board**.  Repeat the process to add the next 2 iterations to your board.

1. Drag and drop the **_Configure DAST Scan_** issue to the current iteration.  Then click the issue name to open its details page.  You will see that the issue is now scheduled for the current iteration.  

1. Click the iteration name to be brought to the iteration details page.   Notice that the iteration, similar to the milestone, has a burndown and burnup chart and shows the list of issues scheduled into the iteration at the bottom of the page.
